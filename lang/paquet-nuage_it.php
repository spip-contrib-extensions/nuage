<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-nuage?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'nuage_description' => 'Questo plugin consente di visualizzare le parole chiave in blocco, variando la dimensione del carattere in base al numero di articoli associati alla parola chiave. In altre parole, Spip ti permette di creare "tag"!

Diversi modelli disponibili:
-* article_nuage : visualizza le parole chiave dell’articolo, la dimensione variabile in base alla frequenza delle parole negli articoli sul sito;
-* rubrique_nuage : visualizza le parole chiave degli articoli del ramo, la dimensione variabile in base alla frequenza delle parole negli articoli di questo ramo;
-* nuage_popularite : visualizza le parole chiave del sito (o di un gruppo), la dimensione variabile in base alla sua relativa popolarità (somma della popolarità degli articoli con questa parola)',
	'nuage_slogan' => 'Visualizza le parole chiave variando la dimensione del carattere'
);
